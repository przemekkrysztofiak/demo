package pl.pkrysztofiak.demo.drawingtools;

import javafx.event.Event;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import pl.pkrysztofiak.demo.drawingtools.tools.LineTool;
import pl.pkrysztofiak.demo.drawingtools.tools.Tool;

public class View extends VBox {

    private final ToggleButton lineButton = new ToggleButton("Line");
    private final ToggleButton curveButton = new ToggleButton("Curve");
    private final HBox tools = new HBox(lineButton, curveButton);
    private final Pane pane = new Pane();
    private final Tool tool = new LineTool(pane);

    public View() {
        pane.setPrefSize(800, 600);
        getChildren().addAll(tools, pane);
        pane.addEventHandler(Event.ANY, event -> tool.handle(event));

    }
}