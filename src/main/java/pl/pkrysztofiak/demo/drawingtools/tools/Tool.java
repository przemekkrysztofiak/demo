package pl.pkrysztofiak.demo.drawingtools.tools;

import javafx.event.Event;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public abstract class Tool {

    protected final Pane pane;

    public Tool(Pane pane) {
        this.pane = pane;
    }

    public abstract void handle(Event event);
}
