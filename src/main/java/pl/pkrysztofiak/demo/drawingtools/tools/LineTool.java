package pl.pkrysztofiak.demo.drawingtools.tools;

import javafx.event.Event;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.util.Optional;

public class LineTool extends Tool {

    private Line line;

    public LineTool(Pane pane) {
        super(pane);
    }

    @Override
    public void handle(Event event) {
        switch (event) {
            case MouseEvent mouseEvent:
                if (MouseEvent.MOUSE_CLICKED.equals(mouseEvent.getEventType())) {
                    if (line == null) {
                        line = new Line();
                        line.setStartX(mouseEvent.getX());
                        line.setStartY(mouseEvent.getY());
                    } else {
                        line = null;
                    }
                } else if (MouseEvent.MOUSE_MOVED.equals(mouseEvent.getEventType())) {
                    if (line != null) {
                        line.setEndX(mouseEvent.getX());
                        line.setEndY(mouseEvent.getY());
                        if (!pane.getChildren().contains(line)) {
                            pane.getChildren().add(line);
                        }
                    }
                }
                break;
            default:
        }
    }
}