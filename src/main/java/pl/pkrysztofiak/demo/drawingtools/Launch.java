package pl.pkrysztofiak.demo.drawingtools;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Launch extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene = new Scene(new View());
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}