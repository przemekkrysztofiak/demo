package pl.pkrysztofiak.demo;

import reactor.core.publisher.Flux;

import java.util.concurrent.TimeUnit;

public class SubscriptionApp {

    public static void main(String[] args) throws InterruptedException {
        Flux.just(1, 2, 3, 4)
                .doOnNext(next -> {
                    System.out.println("next = " + next);
                    if (next == 2) {
                        throw new RuntimeException();
                    }
                })
                .onErrorContinue((e, next) -> {
                    e.printStackTrace();
                    System.out.println("Hello");
                })
                .subscribe();

//        TimeUnit.SECONDS.sleep(4);
    }
}
