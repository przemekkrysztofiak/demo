package pl.pkrysztofiak.demo.assoc;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.data.VR;
import org.dcm4che3.net.*;
import org.dcm4che3.net.pdu.AAssociateRQ;
import org.dcm4che3.net.pdu.PresentationContext;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        Device remoteDevice = new Device("mis60-pacs");

        Connection remoteConnection = new Connection();
        remoteConnection.setHostname("mis60chazon.intra.pixel.com.pl");
        remoteConnection.setPort(4007);

        ApplicationEntity remoteAE = new ApplicationEntity("expacs");
        remoteAE.addConnection(remoteConnection);

        remoteDevice.addConnection(remoteConnection);

        remoteDevice.addApplicationEntity(remoteAE);

        Device localDevice = new Device("theia-device");
        ApplicationEntity localAE = new ApplicationEntity("theia");
        Connection localConnection = new Connection();

        localDevice.addConnection(localConnection);

        localDevice.addApplicationEntity(localAE);
        localAE.addConnection(localConnection);

        localDevice.setExecutor(Executors.newSingleThreadExecutor());
        localDevice.setScheduledExecutor(Executors.newSingleThreadScheduledExecutor());

        try {
            AAssociateRQ rq = new AAssociateRQ();

            rq.addPresentationContext(new PresentationContext(1, UID.StudyRootQueryRetrieveInformationModelFind, UID.ExplicitVRLittleEndian, UID.ExplicitVRBigEndian, UID.ImplicitVRLittleEndian));
            Association association = localAE.connect(remoteAE, rq);

            association.getAAssociateAC().getPresentationContexts().forEach(presentationContext -> {
                System.out.println(presentationContext);
                if (presentationContext.isAccepted()) {
                    System.out.println("accepted");
                }
            });
            Attributes attributes = new Attributes();
            attributes.setString(Tag.QueryRetrieveLevel, VR.CS, "IMAGE");
            attributes.setString(Tag.StudyInstanceUID, VR.UI, "1.3.6.1.4.1.22410.22021819357727619723237362263043808011");
            attributes.setNull(Tag.SOPInstanceUID, VR.UI);

            DimseRSPHandler rspHandler = new DimseRSPHandler(association.nextMessageID()) {

                @Override
                public void onDimseRSP(Association as, Attributes cmd, Attributes data) {
                    super.onDimseRSP(as, cmd, data);
                    int status = cmd.getInt(Tag.Status, -1);
                    if (Status.isPending(status)) {
                        System.out.println(data);
                    }
                }
            };
            association.cfind(UID.StudyRootQueryRetrieveInformationModelFind, Priority.NORMAL, attributes, null, rspHandler);

//            association.release();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (IncompatibleConnectionException e) {
            throw new RuntimeException(e);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }

    }
}
