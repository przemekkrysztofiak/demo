package pl.pkrysztofiak.demo;

public class DicomService {

    public <T extends Number> FindOperation<T> find(String level) {
        if (level == "STUDY") {
            return new FindOperation<>();
        }
        return null;
    }

}