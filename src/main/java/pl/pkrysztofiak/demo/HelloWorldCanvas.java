package pl.pkrysztofiak.demo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class HelloWorldCanvas extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Create a canvas of size 400x200
        Canvas canvas = new Canvas(400, 200);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        // Set background to white
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        // Draw "Hello world!" with yellow fill and black stroke
        gc.setFill(Color.YELLOW);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(0.5);
        gc.setFont(javafx.scene.text.Font.font(24));
        String text = "Hello world!";
        double x = (canvas.getWidth() - javafx.scene.text.Font.font(24).getSize() * text.length() / 2) / 2;
        double y = canvas.getHeight() / 2;
        gc.fillText(text, x, y);
        gc.strokeText(text, x, y);

        StackPane root = new StackPane();
        root.getChildren().add(canvas);

        Scene scene = new Scene(root, 400, 200);

        primaryStage.setTitle("Hello World Canvas");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
