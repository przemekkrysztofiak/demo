package pl.pkrysztofiak.demo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.File;

public class DragAndDropExample extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Pane root = new Pane();
        DraggablePanel panel1 = new DraggablePanel(new FolderDropHandler());
        DraggablePanel panel2 = new DraggablePanel(new PanelDropHandler());

        panel1.setLayoutX(50);
        panel1.setLayoutY(50);
        panel2.setLayoutX(200);
        panel2.setLayoutY(50);

        root.getChildren().addAll(panel1, panel2);

        Scene scene = new Scene(root, 400, 300);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Drag and Drop Example");
        primaryStage.show();
    }
}

class DraggablePanel extends StackPane {
    private final DropHandler dropHandler;

    DraggablePanel(DropHandler dropHandler) {
        this.dropHandler = dropHandler;
        this.setPrefSize(100, 100);
        this.setStyle("-fx-background-color: #aaaaaa; -fx-border-color: black;");
        initializeDragEvents();
    }

    private void initializeDragEvents() {
        this.setOnDragOver(dropHandler::handleDragOver);
        this.setOnDragDropped(dropHandler::handleDragDropped);
    }
}

interface DropHandler {
    void handleDragOver(javafx.scene.input.DragEvent event);
    void handleDragDropped(javafx.scene.input.DragEvent event);
}

class FolderDropHandler implements DropHandler {
    @Override
    public void handleDragOver(javafx.scene.input.DragEvent event) {
        event.getDragboard().getString();
        // Implementation for handling drag over with folders
    }

    @Override
    public void handleDragDropped(javafx.scene.input.DragEvent event) {
        // Implementation for handling drag drop with folders
        if (event.getDragboard().hasFiles()) {
            File file = event.getDragboard().getFiles().get(0);
            if (file.isDirectory()) {
                System.out.println("Folder dropped: " + file.getAbsolutePath());
            }
        }
    }
}

class PanelDropHandler implements DropHandler {
    @Override
    public void handleDragOver(javafx.scene.input.DragEvent event) {
        // Implementation for handling drag over with panels
    }

    @Override
    public void handleDragDropped(javafx.scene.input.DragEvent event) {
        // Implementation for handling drag drop with panels
        System.out.println("Panel dropped onto another panel");
    }
}
